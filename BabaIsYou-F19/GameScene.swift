//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var baba:SKSpriteNode!
     var stopblock:SKSpriteNode!
     var isblock:SKSpriteNode!
     var isblock2:SKSpriteNode!
    var wallblock:SKSpriteNode!
    var flagblock:SKSpriteNode!
     var winblock:SKSpriteNode!
    var wall:SKSpriteNode!
    var wall1:SKSpriteNode!
    var wall2:SKSpriteNode!
    var wall3:SKSpriteNode!
    var flag:SKSpriteNode!

    let PLAYER_SPEED:CGFloat = 20
    
    let approxNumber = 10

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        self.baba = self.childNode(withName: "baba") as? SKSpriteNode
            self.stopblock = self.childNode(withName: "stopblock") as? SKSpriteNode
            self.isblock = self.childNode(withName: "isblock") as? SKSpriteNode
            self.isblock2 = self.childNode(withName: "isblock2") as? SKSpriteNode
            self.wallblock = self.childNode(withName: "wallblock") as? SKSpriteNode
            self.flagblock = self.childNode(withName: "flagblock") as? SKSpriteNode
            self.winblock = self.childNode(withName: "winblock") as? SKSpriteNode
            self.wall = self.childNode(withName: "wall") as? SKSpriteNode
            self.wall1 = self.childNode(withName: "wall1") as? SKSpriteNode
            self.wall2 = self.childNode(withName: "wall2") as? SKSpriteNode
            self.wall3 = self.childNode(withName: "wall3") as? SKSpriteNode
            self.flag = self.childNode(withName: "flag") as? SKSpriteNode
                              
            //baba
            self.baba.physicsBody = SKPhysicsBody(rectangleOf: baba.size)
            self.baba.physicsBody?.affectedByGravity = false
            self.baba.physicsBody?.categoryBitMask = 1
            self.baba.physicsBody?.collisionBitMask = 251
            self.baba.physicsBody?.contactTestBitMask = 0
            self.baba.physicsBody?.allowsRotation = false
                             
           //pushable
            self.winblock.physicsBody = SKPhysicsBody(rectangleOf: winblock.size)
            self.winblock.physicsBody?.affectedByGravity = false
            self.winblock.physicsBody?.categoryBitMask = 2
            self.winblock.physicsBody?.collisionBitMask = 255
            self.winblock.physicsBody?.contactTestBitMask = 159
            self.winblock.physicsBody?.allowsRotation = false
            
            self.stopblock.physicsBody = SKPhysicsBody(rectangleOf: stopblock.size)
            self.stopblock.physicsBody?.affectedByGravity = false
            self.stopblock.physicsBody?.categoryBitMask = 4
            self.stopblock.physicsBody?.collisionBitMask = 255
            self.stopblock.physicsBody?.contactTestBitMask = 159
            self.stopblock.physicsBody?.allowsRotation = false

            self.flagblock.physicsBody = SKPhysicsBody(rectangleOf: flagblock.size)
            self.flagblock.physicsBody?.affectedByGravity = false
            self.flagblock.physicsBody?.categoryBitMask = 8
            self.flagblock.physicsBody?.collisionBitMask = 255
            self.flagblock.physicsBody?.contactTestBitMask = 159
            self.flagblock.physicsBody?.allowsRotation = false
            
            self.wallblock.physicsBody = SKPhysicsBody(rectangleOf: wallblock.size)
            self.wallblock.physicsBody?.affectedByGravity = false
            self.wallblock.physicsBody?.categoryBitMask = 16
            self.wallblock.physicsBody?.collisionBitMask = 255
            self.wallblock.physicsBody?.contactTestBitMask = 159
            self.wallblock.physicsBody?.allowsRotation = false

            //non-pushable
            self.enumerateChildNodes(withName: "isblock") {
              (node, stop) in
              let isblock = node as! SKSpriteNode
              isblock.physicsBody = SKPhysicsBody(rectangleOf: isblock.size)
              isblock.physicsBody?.affectedByGravity = false
              isblock.physicsBody?.isDynamic = false
              isblock.physicsBody?.categoryBitMask = 2
                isblock.physicsBody?.collisionBitMask = 159
                
          }
        self.enumerateChildNodes(withName: "isblock2") {
                (node, stop) in
                let isblock2 = node as! SKSpriteNode
                isblock2.physicsBody = SKPhysicsBody(rectangleOf: isblock2.size)
                isblock2.physicsBody?.affectedByGravity = false
                isblock2.physicsBody?.isDynamic = false
                isblock2.physicsBody?.categoryBitMask = 2
                isblock2.physicsBody?.collisionBitMask = 159
                }
            //walls
            self.enumerateChildNodes(withName: "wall") {
                (node, stop) in
                let wall = node as! SKSpriteNode
                wall.physicsBody = SKPhysicsBody(rectangleOf: wall.size)
                wall.physicsBody?.affectedByGravity = false
                wall.physicsBody?.categoryBitMask = 64
                wall.physicsBody?.collisionBitMask = 159
                wall.physicsBody?.isDynamic = false
                wall.physicsBody?.allowsRotation = false
          }
        
        self.enumerateChildNodes(withName: "wall1") {
                     (node, stop) in
                     let wall1 = node as! SKSpriteNode
                     wall1.physicsBody = SKPhysicsBody(rectangleOf: wall1.size)
                     wall1.physicsBody?.affectedByGravity = false
                     wall1.physicsBody?.categoryBitMask = 64
                     wall1.physicsBody?.collisionBitMask = 159
                     wall1.physicsBody?.isDynamic = false
                     wall1.physicsBody?.allowsRotation = false
               }
        self.enumerateChildNodes(withName: "wall2") {
                     (node, stop) in
                     let wall2 = node as! SKSpriteNode
                     wall2.physicsBody = SKPhysicsBody(rectangleOf: wall2.size)
                     wall2.physicsBody?.affectedByGravity = false
                     wall2.physicsBody?.categoryBitMask = 64
                     wall2.physicsBody?.collisionBitMask = 159
                     wall2.physicsBody?.isDynamic = false
                     wall2.physicsBody?.allowsRotation = false
               }
        self.enumerateChildNodes(withName: "wall3") {
              (node, stop) in
              let wall3 = node as! SKSpriteNode
              wall3.physicsBody = SKPhysicsBody(rectangleOf: wall3.size)
              wall3.physicsBody?.affectedByGravity = false
              wall3.physicsBody?.categoryBitMask = 64
              wall3.physicsBody?.collisionBitMask = 159
              wall3.physicsBody?.isDynamic = false
              wall3.physicsBody?.allowsRotation = false
        }
                self.flag.physicsBody = SKPhysicsBody(rectangleOf: flag.size)
                self.flag.physicsBody?.affectedByGravity = false
                self.flag.physicsBody?.categoryBitMask = 128
                self.flag.physicsBody?.collisionBitMask = 0
                self.flag.physicsBody?.contactTestBitMask = 159
                self.flag.physicsBody?.allowsRotation = false
//                print(flagblock.position.x)
//                print(winblock.position.x)
      
    }
 //MARK: Main logic start from here
    
    override func update(_ currentTime: TimeInterval) {
        
        
        //when wall is solid
        if(Int(wallblock.position.x) - Int(isblock.position.x) == -64 &&
            Int(isblock.position.x) - Int(stopblock.position.x) == -64 &&
            (Int(wallblock.position.y) < Int(isblock.position.y)+approxNumber) &&
            (Int(wallblock.position.y) > Int(isblock.position.y)-approxNumber) &&
            (Int(stopblock.position.y) < Int(isblock.position.y)+approxNumber) &&
            (Int(stopblock.position.y) > Int(isblock.position.y)-approxNumber))
        {
           print("baba do not cross")
             self.baba.physicsBody?.collisionBitMask = 255
     
}
        else
{
        print("baba can cross")
        self.stopblock.physicsBody?.collisionBitMask = 191
            self.winblock.physicsBody?.collisionBitMask = 191
            self.flagblock.physicsBody?.collisionBitMask = 191
            self.wallblock.physicsBody?.collisionBitMask = 191
            self.baba.physicsBody?.collisionBitMask = 191
    //flag is win condition
    if( Int(flagblock.position.x) - Int(isblock2.position.x) == -64 &&
            Int(isblock2.position.x) - Int(winblock.position.x) == -64 &&
            (Int(flagblock.position.y) < Int(isblock2.position.y)+approxNumber) &&
            (Int(flagblock.position.y) > Int(isblock2.position.y)-approxNumber) &&
            (Int(winblock.position.y) < Int(isblock2.position.y)+approxNumber) &&
            (Int(winblock.position.y) > Int(isblock2.position.y)-approxNumber)
       )
                          {
            self.flag.physicsBody?.collisionBitMask = 0
            if(baba.frame.intersects(flag.frame) == true){
            print("congratulations you won")
  }
    else{ print("win pattern deteched just touch flag now")
                            }
                    }
    else{
        print("no win pattern detected")
    }
           
              }
        //when flag is solid
         if( Int(flagblock.position.x) - Int(isblock.position.x) == -64 && Int(isblock.position.x) - Int(stopblock.position.x) == -64 &&
            (Int(flagblock.position.y) < Int(isblock.position.y)+approxNumber) &&
            (Int(flagblock.position.y) > Int(isblock.position.y)-approxNumber) &&
            (Int(stopblock.position.y) < Int(isblock.position.y)+approxNumber) &&
            (Int(stopblock.position.y) > Int(isblock.position.y)-approxNumber)
)
      {
        print("flag is solid ")
        
        self.flag.physicsBody?.collisionBitMask = 0
         }
         else{
            self.flag.physicsBody?.collisionBitMask = 255
        }
        
        //wall is win condition
          if( Int(wallblock.position.x) - Int(isblock2.position.x) == -64 &&
              Int(isblock2.position.x) - Int(winblock.position.x) == -64 &&
              (Int(wallblock.position.y) < Int(isblock2.position.y)+approxNumber) &&
              (Int(wallblock.position.y) > Int(isblock2.position.y)-approxNumber) &&
              (Int(winblock.position.y) < Int(isblock2.position.y)+approxNumber) &&
              (Int(winblock.position.y) > Int(isblock2.position.y)-approxNumber)
             )
                                {
              print("nooooooooo")
              if(self.baba.frame.intersects(wall.frame) == true){
              print("congratulations you won")
        }
              else if(self.baba.frame.intersects(wall1.frame) == true){
                print("congratulations you won")
              }
            else if(self.baba.frame.intersects(wall2.frame) == true){
                print("congratulations you won")
                             }
            else if(self.baba.frame.intersects(wall3.frame) == true){
                print("congratulations you won")
                             }
          else{ print("no win")
                                  }
        }
        func didBegin(_ contact: SKPhysicsContact) {
             let nodeA = contact.bodyA.node
             let nodeB = contact.bodyB.node
             
             if (nodeA == nil || nodeB == nil) {
                 return
             }
             print("COLLISION DETECTED")
             print("Sprite 1: \(nodeA!.name)")
             print("Sprite 2: \(nodeB!.name)")
             print("------")


         }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let mouseTouch = touches.first
               if (mouseTouch == nil) {
                   return
               }
               let location = mouseTouch!.location(in: self)
               
               // WHAT NODE DID THE PLAYER TOUCH
               // ----------------------------------------------
               let nodeTouched = atPoint(location).name
               //print("Player touched: \(nodeTouched)")
               
               
               // GAME LOGIC: Move player based on touch
               if (nodeTouched == "upButton") {
                   // move up
                  //print("Player touched: \(nodeTouched)")
                   self.baba.position.y = self.baba.position.y + PLAYER_SPEED
               }
               else if (nodeTouched == "downButton") {
                   // move down
                   self.baba.position.y = self.baba.position.y - PLAYER_SPEED
               }
               else if (nodeTouched == "leftButton") {
                   // move left
                   self.baba.position.x = self.baba.position.x - PLAYER_SPEED
               }
               else if (nodeTouched == "rightButton") {
                   // move right
                   self.baba.position.x = self.baba.position.x + PLAYER_SPEED
               }
               
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
